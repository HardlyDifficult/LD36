﻿using UnityEngine;



public class BearStateController : MonoBehaviour {

    public Animator anim;

    public BearState animState;

    private bool walk = false;
    private bool run = false;
    private bool bite = false;
    private bool slash = false;
    private bool stand = false;
    private bool receiveHit = false;
    private bool death = false;

    void Awake() {
        animState = BearState.Walk;
    }

    // Update is called once per frame
    void Update() {

        switch ( animState ) {
            case BearState.Walk: walk = true; anim.SetBool("walk" , walk); walk = false;  break;
            case BearState.Run: run = true;  anim.SetBool("run" , run); run = false;  break;
            case BearState.Bite: bite = true;  anim.SetBool("bite" , bite); bite = false;  break;
            case BearState.Slash: slash = true;  anim.SetBool("slash" , slash); slash = false; break;
            case BearState.Stand: stand = true;  anim.SetBool("stand" , stand); stand = false;  break;
            case BearState.ReceiveHit: receiveHit = true; anim.SetBool("receiveHit" , receiveHit); receiveHit = false; break;
            case BearState.Death: death = true; anim.SetBool("death" , death); death = false;  break;
            default : anim.SetBool("walk" , walk); break;
        }
    }   
}
