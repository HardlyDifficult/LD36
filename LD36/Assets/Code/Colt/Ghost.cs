﻿using System.Collections;
using UnityEngine;

public class Ghost : MonoBehaviour {
    public float invisibilityTime = 0.75f;
    public float scareDistance = 7;
    public float scareTime = 5;
    public float swapTime = 20;
    bool iHaveAHome;
    float invisibility = 0;
    LevelController levelController;
    Renderer renderer;
    float swapTimer = 0;

    public Vector3 Boo(Vector3 position) {
        renderer.enabled = true;
        invisibility = invisibilityTime;
        transform.rotation = Quaternion.LookRotation((transform.position - position).normalized);

        NavMeshHit newDestination;
        NavMesh.SamplePosition(transform.position + (new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * scareDistance), out newDestination, 10, 1);
        return newDestination.position;
    }

    void Start() {
        levelController = Camera.main.GetComponent<LevelController>();
        Debug.Assert(levelController != null);
        renderer = GetComponentInChildren<Renderer>();
        renderer.enabled = false;
        Debug.Assert(renderer != null);
        Swap();
    }

    void Swap() {
        for(int i = 0; i < levelController.liveChunks.Count; i++) {
            var trees = levelController.liveChunks[i].GetComponentsInChildren<TreeRessource>();
            for(int j = 0; j < trees.Length; j++) {
                var random = UnityEngine.Random.Range(0, 5);
                if(random == 1) {
                    Debug.Log("Spirit found a tree");
                    transform.position = trees[j].transform.position + (new Vector3(0, 5, 0));
                    transform.SetParent(trees[j].transform);
                    renderer.enabled = false;
                    iHaveAHome = true;
                    return;
                }
            }
        }
    }

    void Update() {
        if(swapTimer > swapTime) {
            swapTimer = 0;
            Swap();
        }
        swapTimer += Time.deltaTime;

        if(iHaveAHome) {
            if(invisibility > 0)
                invisibility -= Time.deltaTime;
            else if(invisibility < 0)
                invisibility = 0;
            if(renderer.enabled && invisibility == 0)
                renderer.enabled = false;

            for(int i = 0; i < TemperatureManager.me.workers.Count; ++i) {
                BasicWorker worker = TemperatureManager.me.workers[i];
                float distance = (transform.position - worker.transform.position).magnitude;
                if(distance < scareDistance) {
                    worker.ResourceCollectionComplete();
                    worker.myMovement.MoveTo(Boo(worker.transform.position));
                }
            }
        }
    }
}