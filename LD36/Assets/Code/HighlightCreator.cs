﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class HighlightCreator {
    static Material highlightMaterial;

    static HighlightCreator() {
        highlightMaterial = Resources.Load<Material>("HighlightMaterial");
        Debug.Assert(highlightMaterial != null);
    }

    internal static GameObject Create(GameObject gameObject) {
        var highlight = (GameObject)MonoBehaviour.Instantiate(gameObject, gameObject.transform);
        highlight.transform.localScale = Vector3.one * 1.2f;

        RemoveThings(highlight);
        UpdateChildren(highlight);

        return highlight;
    }

    private static void CleanUpComponents<T>(T[] allComponents) where T : UnityEngine.Object {
        for(int i = 0; i < allComponents.Length; i++) {
            if(!(allComponents[i] is Transform)) {
                if(!(allComponents[i] is MeshRenderer)) {
                    MonoBehaviour.Destroy(allComponents[i]);
                } else {
                    var count = (allComponents[i] as MeshRenderer).materials.Length;
                    var newMaterials = new Material[count];
                    for(int iMat = 0; iMat < newMaterials.Length; iMat++) {
                        newMaterials[iMat] = highlightMaterial;
                    }
                    (allComponents[i] as MeshRenderer).materials = newMaterials;
                    //for(int iMat = 0; iMat < (allComponents[i] as MeshRenderer).materials.Length; iMat++) {
                    //    (allComponents[i] as MeshRenderer).materials[iMat] = highlightMaterial;
                    //}
                }
            }
        }
    }

    private static void RemoveThings(GameObject highlight) {
        CleanUpComponents(highlight.GetComponents<Component>());
    }

    private static void UpdateChildren(GameObject highlight) {
        for(int i = 0; i < highlight.transform.childCount; i++) {
            RemoveThings(highlight.transform.GetChild(i).gameObject);
            UpdateChildren(highlight.transform.GetChild(i).gameObject);
        }
    }
}