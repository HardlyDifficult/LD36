using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour {
    public GameObject firePit;
    public GameObject firePitPerLightning;
    public float introLengthInSeconds;
    float effectFadeTime = 0.1f;
    [Header("Light Effects")]
    [SerializeField]
    Image effectImage;
    // TODO Maybe flash a light as well
    [SerializeField]
    GameObject[] lightningBolts;

    private IEnumerator DisplayLightning() {
        yield return new WaitForSeconds(9);
        while(introLengthInSeconds > 0) {
            var selectedBolt = UnityEngine.Random.Range(0, lightningBolts.Length);
            lightningBolts[selectedBolt].SetActive(true);
            LightningLightEffect(); // Play lightning light effect
            var sleepTime = UnityEngine.Random.Range(.1f, .5f);
            introLengthInSeconds -= sleepTime;
            yield return new WaitForSeconds(sleepTime);
            lightningBolts[selectedBolt].SetActive(false);
            sleepTime = UnityEngine.Random.Range(.01f, .2f);
            introLengthInSeconds -= sleepTime;
            yield return new WaitForSeconds(sleepTime);
        }
        firePitPerLightning.SetActive(false);
        firePit.SetActive(true);
        var campFire = GameObject.Find("CampFire").GetComponent<CampfireFueled>();
        campFire.StartTheFire();
        Destroy(gameObject);
    }

    IEnumerator LightFadeOut() {
        effectImage.enabled = true;

        float time = effectFadeTime;
        Color originalColor = effectImage.color;
        float originalAlpha = originalColor.a;

        while(time > 0) {
            Color newColor = new Color(originalColor.r, originalColor.g, originalColor.b, time / effectFadeTime * originalAlpha);
            effectImage.color = newColor;

            time -= Time.deltaTime;
            yield return null;
        }

        effectImage.color = originalColor;

        effectImage.enabled = false;
    }

    private void LightningLightEffect() {
        StartCoroutine(LightFadeOut());
    }

    // Use this for initialization
    void Start() {
        /*
        lightningBolts = new GameObject[transform.childCount];
        for(int i = 0; i < lightningBolts.Length; i++) {
            lightningBolts[i] = transform.GetChild(i).gameObject;
            lightningBolts[i].SetActive(false);
        }
        */
        StartCoroutine(DisplayLightning());
    }
}