﻿using System;
using UnityEngine;

public static class SharedResources {
    static GameObject soundEffectPlayer;

    static SharedResources() {
        soundEffectPlayer = (GameObject)Resources.Load("SoundEffectPlayer");
    }

    public static GameObject PlaySoundEffect(AudioClip clip, float volume, float pitch = 1) {
        var newPlayer = (GameObject)MonoBehaviour.Instantiate(soundEffectPlayer, Vector3.zero,
            Quaternion.identity, Camera.main.transform);
        var audio = newPlayer.GetComponent<AudioSource>();
        audio.clip = clip;
        audio.volume = volume;
        audio.pitch = pitch;
        audio.Play();

        return newPlayer;
    }
}