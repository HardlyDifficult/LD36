﻿using System;
using System.Collections;
using UnityEngine;

public class SoundEffectPlayer : MonoBehaviour {

    private IEnumerator DieWhenClipIsOver() {
        yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
        Destroy(gameObject);
    }

    void Start() {
        StartCoroutine(DieWhenClipIsOver());
    }
}