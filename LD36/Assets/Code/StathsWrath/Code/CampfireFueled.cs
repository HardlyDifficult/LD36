﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CampfireFueled : Ressource {
    public GameObject fireEffect;
    public float initialFuel = 60;
    /// <summary>
    /// amount of fuel burned in a second
    /// </summary>
    public float rateOfBurn = 1;
    public Image uiFire;
    public GameObject uiGameOver;
    /// <summary>
    /// actual fuel
    /// </summary>
    float _totalFuel;
    /// <summary>
    /// current fuel objects in the fire
    /// </summary>
    CampFireFlickerLight flicker;
    private List<Fuel> fuelOnFire = new List<Fuel>();
    Light light;
    public int RemainingFuel {
        get {
            return (int)totalFuel;
        }
    }
    private float totalFuel {
        get {
            return _totalFuel;
        }
        set {
            _totalFuel = value;

            var targetFlame = Math.Min(10, Math.Max(0, _totalFuel / 100));
            uiFire.fillAmount = targetFlame / 10;
            if(targetFlame == 0) {
                Destroy(light);
                Destroy(fireEffect);
            } else {
                var max = targetFlame * .8f;
                light.range = targetFlame * 2;
                flicker.maxFlickerIntensity = max;
                flicker.minFlickerIntensity = max * .2f;
                fireEffect.transform.localScale = new Vector3(1, 1, targetFlame / 3f);
            }
        }
    }

    public void AddFuel(Fuel _fuel) {
        fuelOnFire.Add(_fuel);
    }

    public void StartTheFire() {
        fireEffect.SetActive(true);
        light = fireEffect.transform.FindChild("Light").GetComponent<Light>();
        flicker = GetComponent<CampFireFlickerLight>();
        Debug.Assert(light != null);
        totalFuel = initialFuel;
        StartCoroutine(Burn());
    }

    protected override void HarvestActionComplete(BasicWorker worker) {
        Fuel fuel;
        if(worker.inventory.TryRemoveFirstItemOfType<Fuel>(out fuel)) {
            print("fuel added to the fire");
            AddFuel(fuel);
        }

        HarvestCompleteNoLoot();
    }

    private IEnumerator Burn() {
        while(totalFuel > 0) {
            for(int i = fuelOnFire.Count - 1; i >= 0; i--) {
                totalFuel += fuelOnFire[i].Consume();
                if(fuelOnFire[i].IsConsumed)
                    fuelOnFire.Remove(fuelOnFire[i]);
            }

            totalFuel -= rateOfBurn;

            yield return new WaitForSeconds(1);
        }

        uiGameOver.SetActive(true);
        Debug.LogError("GG, out of fuel in the campfire");
    }
}