﻿using UnityEngine;
using System.Collections.Generic;

namespace Credits
{
    public class CreditsManager : MonoBehaviour
    {
        //a List of all of the possible spawn points
        public List<SpawnpointScript> spawnPoints;

        void Start()
        {
            for (int i = 0; i < CreditsDictionary.creditsDictionary.Count; i++)
            {
                int pointToUse = Random.Range(0, spawnPoints.Count);

                spawnPoints[pointToUse].dictonaryIndexes.Add(i);
            }
        }
    }
}