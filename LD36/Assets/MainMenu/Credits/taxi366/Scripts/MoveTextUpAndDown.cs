﻿using UnityEngine;
using UnityEngine.UI;

namespace Credits {
    public class MoveTextUpAndDown : MonoBehaviour {
        /*
        This just moves the text up and down so it looks like it is comming out if the trees
        */

        //the point that spawned the text
        [System.NonSerialized]
        public SpawnpointScript mySpawnpoint;
        //the 2 text components on the prefab i am adding then here becau it is more efficent than finding them i believe
        public Text nameText;
        public Text roleText;
        uint countdown = 40;
        private bool? shouldMoveDown;
        //the point that the text is spawned at;
        private Vector3 startingPosition;

        void FixedUpdate() {
            MoveText();
        }

        void MoveText() {
            //Sorry this is a mess HD dont hurt me Pls <3

            if(!shouldMoveDown.HasValue) {
                if(startingPosition.y + 9.5 > gameObject.transform.position.y) {
                    gameObject.transform.position += new Vector3(0, 5, 0) * Time.deltaTime;
                } else {
                    shouldMoveDown = false;
                }
            } else if(shouldMoveDown.Value) {
                if(startingPosition.y - 10 > gameObject.transform.position.y) {
                    if(mySpawnpoint) {
                        mySpawnpoint.canSpawnNewText = true;
                        mySpawnpoint.SpawnDelay();

                        Destroy(gameObject);
                    }
                } else {
                    gameObject.transform.position -= new Vector3(0, 5, 0) * Time.deltaTime;
                }
            } else {
                countdown--;
                if(countdown == 0) {
                    shouldMoveDown = true;
                }
            }
        }

        void Start() {
            startingPosition = gameObject.transform.position;
            shouldMoveDown = null;
        }
    }
}