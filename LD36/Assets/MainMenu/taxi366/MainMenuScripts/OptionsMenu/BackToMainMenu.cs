﻿using UnityEngine;
using System.Collections;

public class BackToMainMenu : MonoBehaviour
{
    public GameObject mainMenu;

    void Awake()
    {
        mainMenu = GameObject.Find("MainMenuCanvas");
    }

    public void ToMainMenu()
    {
        mainMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}